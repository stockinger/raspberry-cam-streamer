╔════════════════════╗
║ CAMERA NODE README ║
╚════════════════════╝
This camera node is able to stream a configurable video stream to a display node.

If a start command is received from the control node, raspivid will be started with the desired options,
as defined in the configuration. The video stream is directly piped to the vlc console plugin,
which opens a http stream, which can be easily accessed by the display node.

If a stop command is received, raspivid and the vlc process will be terminated.

┌──────────────┐
│ cam command: │
└──────────────┘
raspivid -o - -t 0 -hf -b 1000000 -w 640 -h 480 -fps 24 |cvlc -vvv stream:///dev/stdin --sout '#standard{access=http,mux=ts,dst=:8160}' :demux=h264

To run this application, vlc has to be installed and your system should be up to date:
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install vlc

┌──────────────┐
│ how to make: │
└──────────────┘
"make clean" -> remove output files
"make" -> make the cam server

┌──────────────────────────┐
│ how to start the server: │
└──────────────────────────┘
./serverRaspPICam

┌──────────────┐
│ how to test: │
└──────────────┘
- Test cam server and streaming:
  First start "./serverRaspPICam" and then "./clientControlNode".
  The client will send the start command and a video config to the cam server.
  To test the stream, just start your VLC and open a stream rom the <CAM-NODE-x-IP> on port 8160
 
- Unittest for CommandLine.c
    ./UT_commandline
- Unitttest for kill_proc.c
    ./UT_kill_proc

┌──────────────────────────────────────────┐
│ how to run the server at system startup: │
└──────────────────────────────────────────┘
here the /etc/rcl.local method is used.
Additionally, an own autostart script is used to keep the rc.local clean.

- in /etc/rc.local the line "sudo -u pi /home/pi/autostart.sh" is added to load the autostart.sh script at startup with
  privileges of user pi
- create the autostart.sh in the designated folder.
  content of autostart.sh is as follows:
    --------------------------------------
    # sleeps 10 sec, primarily to wait until wi-fi is connected
    sleep 10
    # change working directory to home of user pi
    cd ~
    # starts the server application as user pi, writes output to new file for logging purposes.
    ./Desktop/cam/serverRaspPICam >> /home/pi/Desktop/newfile
    --------------------------------------