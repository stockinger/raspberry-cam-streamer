/*
 * @filename:    CommandLine.h
 * @author:      Stefan Stockinger
 * @date:        2017-01-13
 * @description: generates command for raspivid
*/
#ifndef COMMANDLINE_H_
#define COMMANDLINE_H_

#include <inttypes.h>
#include "messageTypes.h"

#define DEFAULT_WIDTH 200
#define DEFAULT_HEIGTH 200
#define DEFAULT_FPS 15
#define MAX_WIDTH 320
#define MAX_HEIGTH 240
#define MAX_FPS 25
#define MIN_FPS 10
#define MIN_WIDTH 50
#define MIN_HIGHT 50

/**
 * @brief generateConsoleCommand
 *          generates a raspivid console command, that is executable
 *          depending on parameters of camConfig struct
 *              pipes stream to vlc plugin.
 *              call in vlcplayer via: http://<ip_address>:8160
 * @param output_str char array, that contains the desired console command
 * @param len size of output char array, to avoid buffer overflows
 * @param camConfig cam config struct, from which output is generated
 * @return void
 */
void generateConsoleCommand(char * output_str, uint16_t len, cam_config_t camConfig);

#endif
