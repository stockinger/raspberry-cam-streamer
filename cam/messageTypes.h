#ifndef MESSAGETYPES_H_
#define MESSAGETYPES_H_

#include <stdint.h>


//message types Nodes may send
typedef enum {
    AUTH_REQUEST, //request authentication to access camera controls
    AUTH_SET_MASTER_FP = 10, //set new master fingerprint
    AUTH_SET_PASSWORD = 20, // set new password
    DISPLAY_CONFIG = 30, //configure display Node
    CAM_DISP_CONFIG = 40, //configure camera/display Node
    CAM_CTRL, //start/stop message
    DISP_CTRL = 50 //ready/sleep message
} control_message_t;


//node responses upon configuration data
typedef enum {
    REQ, //set by sender, before op has been performed
    SUCCESS = 200, //respond code if operation was succesful
    UNSET = 204, //respond code if no master fingerprint is set
    UNSUPPORTED = 404, //if operation is not supported
    FAIL = 500 //if operation failed
} respond_t;


//header for each message
typedef struct {
    control_message_t mtype; //kind of operation to be performed/has been performed
    respond_t resp; //response on operation
} msg_hdr_t;


//display configuration data
typedef struct {
    uint16_t cam_id; //which camera node to use
    uint16_t res_x; //in pixel
    uint16_t res_y; //in pixel
    uint16_t fps; //pictures in second
} display_config_t;

//camera actions
typedef enum {
    STOP,
    START,
    READY,
    SLEEP
} cam_action_t;

//camera configuration data
typedef struct {
    display_config_t cam_cfg;
} cam_config_t;

//tell camera to perform action
typedef struct {
    uint16_t cam_id;
    cam_action_t action;
} cam_control_t;

//data sent by camera
typedef struct {
    uint16_t cam_id;
    uint8_t segments; //# of picture parts
    uint8_t segment_i; // i^th part
    uint32_t timestamp_low; //low word of timestemp in milliseconds
    uint32_t timestamp_hi; //hi word of timestamp in milliseconds
    uint32_t length; //picture length
    uint8_t *data; //picture
} cam_data_t;

#endif
