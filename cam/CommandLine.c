/*
 * @filename:    CommandLine.c
 * @author:      Stefan Stockinger
 * @date:        2017-01-13
 * @description: generates command for raspivid
*/

#include <stdio.h>
#include "CommandLine.h"

/**
 * @brief generateConsoleCommand
 *          generates a raspivid console command, that is executable
 *          depending on parameters of camConfig struct
 *              pipes stream to vlc plugin.
 *              call in vlcplayer via: http://<ip_address>:8160
 * @param output_str char array, that contains the desired console command
 * @param len size of output char array, to avoid buffer overflows
 * @param camConfig cam config struct, from which output is generated
 * @return void
 *
 * TODO: maybe raspistill for lower framerates? Easy to accomplish, just another if-statement
 */
void generateConsoleCommand(char * output_str, uint16_t len, cam_config_t camConfig) {
    //check struct for validity
    if (camConfig.cam_cfg.res_x < MIN_WIDTH || (camConfig.cam_cfg.res_x > MAX_WIDTH)) {
        //for constant aspect ratio
        camConfig.cam_cfg.res_x = DEFAULT_WIDTH;
        camConfig.cam_cfg.res_y = DEFAULT_HEIGTH;
    }
    if (camConfig.cam_cfg.res_y < MIN_HIGHT || (camConfig.cam_cfg.res_y > MAX_HEIGTH)) {
        //for constant aspect ratio
        camConfig.cam_cfg.res_x = DEFAULT_WIDTH;
        camConfig.cam_cfg.res_y = DEFAULT_HEIGTH;
    }
    if (camConfig.cam_cfg.fps < MIN_FPS || camConfig.cam_cfg.fps > MAX_FPS) {
        camConfig.cam_cfg.fps = DEFAULT_FPS;
    }

    /*
     * RASPIVID USAGE:
     * https://www.raspberrypi.org/documentation/raspbian/applications/camera.md
     *
     * -o -     output to stdout
     * -t 0     capture forever
     * -hf      horizontal flip
     *
     * -w       image width
     * -h       image height

     * -fps     fps
     *
     * other pipe commands -> stream
     * raspivid -o - -t 0 -hf -w 200 -h 100 -fps 25 |cvlc -vvv stream:///dev/stdin --sout '#standard{access=http,mux=ts,dst=:8160}' :demux=h264
     */

    snprintf(output_str, len, "%s %"PRIu16" %s %"PRIu16" %s %"PRIu16" %s%s%s\n",
             "raspivid -o - -t 0 -hf -w",
             camConfig.cam_cfg.res_x,
             "-h",
             camConfig.cam_cfg.res_y,
             "-fps",
             camConfig.cam_cfg.fps,
             "-b 1000000|cvlc -vvv stream:/","/","/dev/stdin --sout '#standard{access=http,mux=ts,dst=:8160}' :demux=h264");
// "|cvlc -vvv stream:/","/","/dev/stdin --sout '#rtp{sdp=rtsp:/","/:8554/,mux=ts}' :demux=h264");


}
