//this file is not needed anymore - just online to have some souveniers of the past! ;)
/*
 * @filename:    udpCamServer.c
 * @author:      Stefan Stockinger
 * @date:        2016-12_28
 * @description: this file contains a udp streaming server only for testcases
*/
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>

#include "kill_proc.h"
#include "CommandLine.h"
#include "messageTypes.h"

#define BUF 256
#define PORTNO 7777
#define CLIADDR 10.0.0.10
#define CLIPORT 55000
/*
///TODO: adjust defines
#define DEFAULT_WIDTH 640
#define DEFAULT_HEIGTH 480
#define DEFAULT_FPS 640
#define MAX_WIDTH 640
#define MAX_HEIGTH 480
#define MAX_FPS 640

*/


int kbhit(void)
{
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    if(ch != EOF)
    {
        ungetc(ch, stdin);
        return 1;
    }

    return 0;
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char*argv[])
{
    cam_config_t camconf;
    int pipede[2];
    if(pipe(pipede) == -1) error("Creating Pipe");
    pid_t procid = fork();
    printf("ProcessID: %d\n", procid );
    if(procid == 0)
    {
        //CHILD PROCESS
        printf("in the child\n");
        //routing stdout to pipe
        int ret = dup2(pipede[1],STDOUT_FILENO);
        printf("New FD: %d\n", ret );
        //closing reading end of pipe
        close(pipede[0]);
        close(pipede[1]);


///TODO: create command string with settings
        /*
        	 char command[200];
        if((camconf.cam_cfg.res_x < 0) || (camconf.cam_cfg.res_x > MAX_WIDTH))
        	camconf.cam_cfg.res_x = DEFAULT_WIDTH;
        if((camconf.cam_cfg.res_y < 0) || (camconf.cam_cfg.res_y > MAX_HEIGTH))
        	camconf.cam_cfg.res_y = DEFAULT_HEIGTH;
        if((camconf.cam_cfg.fps < 0) || (camconf.cam_cfg.res_x > MAX_FPS))
        	camconf.cam_cfg.fps = DEFAULT_FPS;


            sprintf(command,"%s %s %s %s %s %s %s\n", "raspivid -o - -t 0 -hf -w",camconf.cam_cfg.res_x,"-w",camconf.cam_cfg.res_y,"-fps",camconf.cam_cfg.fps,"|cvlc -vvv stream:/\/\/dev/stdin --sout '#standard{access=http,mux=ts,dst=:8160}' :demux=h264");

        */

        //execute raspivid
        //https://www.raspberrypi.org/documentation/raspbian/applications/camera.md
        char command[200];
        camconf.cam_cfg.fps = 30;
        camconf.cam_cfg.res_x = 30;
        camconf.cam_cfg.res_y = 30;

        generateConsoleCommand(command, sizeof(command),camconf);
        ret = system(command);
        if(ret > 0) {
            printf("Cam started");
        }
        else {
            printf("Raspivis error\n");
        }
        proc_find("vlc");
    }
    else if(procid >= 0)
    {
        //PARENT PROCESS
        //wait(NULL);

        int sock, n;
        struct sockaddr_in server;
        struct sockaddr_in from;
        socklen_t fromlen;
        char buffer[BUF];
        int readcount;

        //Create Network Socket
        sock=socket(AF_INET, SOCK_DGRAM, 0);
        if (sock < 0) error("Opening socket");

        //Initialize Memory of Address Struct
        bzero(&server,sizeof(server));
        //Setup Address Struct
        server.sin_family=AF_INET;
        server.sin_addr.s_addr=INADDR_ANY;
        server.sin_port=htons(PORTNO);
        //Bind Address Struct to Socket
        if (bind(sock,(struct sockaddr *)&server,sizeof(server))<0)
            error("binding");

        fromlen = sizeof(struct sockaddr_in);
        //Recieve from Socket and get client address struct "from"
        n = recvfrom(sock,buffer,BUF,0,(struct sockaddr *)&from,&fromlen);
        if (n < 0) error("recvfrom");
        printf("Client Port: %d\n", from.sin_port);
        //close writing end of pipe
        close(pipede[1]);
        int i = 0;
        while(!kbhit()) {
            //reading pipe
            readcount = read(pipede[0],buffer,BUF);
            printf("pipe: %d\n", readcount);
            //Send buffer over Socket
            /*
            //data sent by camera
            		typedef struct {
            		uint16_t cam_id;
            		uint8_t segments; //# of picture parts
            		uint8_t segment_i; // i^th part
            		uint32_t timestamp_low; //low word of timestemp in milliseconds
            		uint32_t timestamp_hi; //hi word of timestamp in milliseconds
            		uint32_t length; //picture length
            		uint8_t *data; //picture
            		} cam_data_t;*/

            sendto(sock,buffer,BUF,0,(struct sockaddr *)&from,fromlen);
            i++;
        }
        kill(procid,SIGTERM);
        printf("stopped\n");
    }
    else
    {
        printf("forking error\n");
    }
    return 0;
}



