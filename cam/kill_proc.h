/*
 * @filename:    kill_proc.h
 * @author:      Stefan Stockinger
 * @date:        2017-01-13
 * @description: returns the pid of a given process
*/
#ifndef KILL_PROC_H
#define KILL_PROC_H


/**
 * @brief  returns the pid of a given process by its name.
 * @param const char* name - name of the process which should be found
 * @return  pid of the process
 *          1, if process was not found,
 *         -1, if  an reading error occured
 */
int proc_find(const char* name);


#endif
