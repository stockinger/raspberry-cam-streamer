/**!
 * @file     serverRaspPICam.c
 * @version  V0.1
 * @date     Jan 13
 * @author   Stefan Stockinger
 */

#define _POSIX_C_SOURCE 200112L
/********************************************* LIBS*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <inttypes.h>
#include <signal.h>
/********************************************* LIBS OWN*/
#include "messageTypes.h"
#include "CommandLine.h"
#include "kill_proc.h"
/********************************************* DEFINES*/
#define TX_BUFFER_SIZE 1024
#define RX_BUFFER_SIZE 1024
#define MAX_CHAR_OUTPUT_STR 200
#define IP_ADDRESS "127.0.0.1"
#define NTW_PORT 7777
#define MAX_PENDING_CONNEECTIONS 10
/********************************************* STATIC*/
static volatile sig_atomic_t execute = 1;
static char output_str[MAX_CHAR_OUTPUT_STR] = {0};
static cam_config_t camConfig;
static char txBuffer[TX_BUFFER_SIZE] = {0};
/********************************************* STATIC-FUNC*/
static _Bool parseMessage(int csocket, char *message);
static _Bool sendResponse(int csocket, msg_hdr_t *msgHeaderResp);
/********************************************* EXIT HANDLER*/
static void exitHandler(int sig, siginfo_t *siginfo, void *ignore);
/********************************************* MAIN*/
int main(int argc , char *argv[]) {
    int opt = 1;
    int iRet, sfd, clientCtrl;
    char rxBuffer[RX_BUFFER_SIZE] = {0};
    //static pid_t pid;
    struct sockaddr_in saiServer;
    struct sigaction act;

    act.sa_flags = SA_SIGINFO;
    act.sa_sigaction = exitHandler;
    sigaction(SIGPIPE, &act, 0);
    sigaction(SIGINT, &act, 0);

    if(getpid() == 0) {
        return 0;
    }
    (void)printf("Raspberry PI CAM server has started!\n");

    //Create a client socket for IPPROTO_TCP
    sfd = socket(AF_INET , SOCK_STREAM , 0);
    if (sfd == -1)	{
        close(sfd);
        (void)printf("Socket creation failed: %s\n", strerror(errno));
        (void)printf("Server has shut down!\n");
        return 1;
    }

    (void)printf("Socket created!\n");

    iRet = setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt));

    //Set the client structure according to the input
    saiServer.sin_family = AF_INET;
    saiServer.sin_addr.s_addr = INADDR_ANY;
    saiServer.sin_port = htons(NTW_PORT);

    iRet = bind(sfd,(struct sockaddr *)&saiServer, sizeof(saiServer));
    if(iRet == -1) {
        close(sfd);
        (void)printf("Socket Bind failed: %s\n", strerror(errno));
        (void)printf("Server has shut down!\n");
        return 1;
    }

    (void)printf("Binding done!\n");

    iRet = listen(sfd, MAX_PENDING_CONNEECTIONS);
    if(iRet == -1) {
        close(sfd);
        (void)printf("Socket Listen failed: %s\n", strerror(errno));
        (void)printf("Server has shut down!\n");
        return 1;
    }

    (void)printf("Listening for incoming requests\n");

    while(execute) {
        (void)printf("Waiting for incoming CAM-Server requests...\n");
        clientCtrl = accept(sfd, NULL, NULL);

        if(clientCtrl < 0) {
            if(errno != EWOULDBLOCK) {
                (void)printf("Server: Accept call failed - Fatal error! %s\n", strerror(errno));
                break;
            }
            break;
        }

        (void)printf("Connection to Client established!\n");

        //keep communication with server
        while(execute) {
            iRet = recv(clientCtrl, rxBuffer, sizeof(rxBuffer), 0);
            if(iRet == -1) {
                (void)printf("Message could not be received %s\n", strerror(errno));
                break;
            } else if (iRet == 0) {
                break;
            }

            parseMessage(clientCtrl, rxBuffer);
            memset(rxBuffer, 0x0, sizeof(rxBuffer));
        }
    }

    (void)printf("Server (%d) has been disconnected!\n", clientCtrl);
    close(sfd);
    return 1;

}

/**
 * @brief parseMessage
 *          parses received message from client (control node)
 * @param sfd socket file descriptor of client
 * @param message message from client
 * @return true if no errors found
 */
static _Bool parseMessage(int sfd, char *message) {
    msg_hdr_t msgHeader;
    pid_t  pid;
    pid_t kill_pid;
    int ret = 0;

    if(message == NULL) {
        exit(0);
    }

    memcpy(&msgHeader, message, sizeof(msg_hdr_t));
    if((msgHeader.mtype == CAM_DISP_CONFIG) && (msgHeader.resp == REQ)) {
        (void)printf("Server: Camera-Display-Config message received!\n");

        memcpy(&camConfig, message + sizeof(msg_hdr_t), sizeof(cam_config_t));
        printf("CONFIG: id: %d, res_x: %d, res_y: %d, fps: %d\n", camConfig.cam_cfg.cam_id, camConfig.cam_cfg.res_x,  camConfig.cam_cfg.res_y,  camConfig.cam_cfg.fps );

        //Send response to control node
        msgHeader.resp = SUCCESS;
        sendResponse(sfd, &msgHeader);
    } else if((msgHeader.mtype == CAM_CTRL) && (msgHeader.resp == REQ)) {
        cam_control_t camCtrl;

        (void)printf("Server: Camera-Control message received! \n");
        memcpy(&camCtrl, message + sizeof(msg_hdr_t), sizeof(cam_control_t));

        switch (camCtrl.action) {
        case STOP:
            kill_pid = proc_find("raspivid");
            if( kill_pid > 0) {
                if(kill(kill_pid, SIGKILL) == 0) {
                    printf("killed raspivid\n");
                }
            }
            kill_pid = proc_find("vlc");
            if( kill_pid > 0) {
                if(kill(kill_pid, SIGKILL) == 0) {
                    printf("killed vlc\n");
                }
            }
            msgHeader.resp = SUCCESS;

            break;
        case START:
            ret = proc_find("raspivid");
            if(ret!=1) {
                msgHeader.resp = FAIL;
            } else {
                msgHeader.resp = SUCCESS;

                pid =fork();
                if(pid == 0) {
                    generateConsoleCommand(output_str, sizeof(output_str), camConfig);
                    (void)system(output_str);
                    (void)printf("Camera stopped!\n");
                    exit( 0);
                }
            }
            break;
        case SLEEP:
            msgHeader.resp = UNSUPPORTED;
            break;
        case READY:
            msgHeader.resp = UNSUPPORTED;

            break;
        default:
            msgHeader.resp = UNSUPPORTED;

            break;
        }

        //Send response to control node
        sendResponse(sfd, &msgHeader);
    }
    else {
        (void)printf("Server: Unknown message received!\n");

        //Send response to control node
        msgHeader.resp = UNSUPPORTED;
        sendResponse(sfd, &msgHeader);
    }
    return true;
}

    /**
     * @brief sendResponse
     *          sends message to client
     * @param csocket socket file descriptor of client
     * @param msgHeaderResp message to send
     * @return true, if successful ;false, if failed
     */
static _Bool sendResponse(int csocket, msg_hdr_t *msgHeaderResp) {
    ssize_t iRet;

    if(msgHeaderResp == NULL) {
        return false;
    }

    memset(txBuffer, 0x0, sizeof(txBuffer));
    memcpy(txBuffer, msgHeaderResp, sizeof(msg_hdr_t));

    iRet = send(csocket , txBuffer , sizeof(txBuffer) , 0);

    if(iRet == -1) {
        (void)printf("Transmission to Control-Node (Client) failed %s\n", strerror(errno));
        return false;
    }
    return true;
}


/**
 * @brief Exit handler for the Signal broken pipe.
 * @param int sig - Signal type which was received
 * @param siginfo_t *siginfo contains additional information
 * about the signal - properties
 * @param void *ignore is not utilized
 *
 * @return void - none
 */
void exitHandler(int sig, siginfo_t *siginfo, void *ignore) {
    (void)printf("Connection to Client lost!"
                 "Received Signal: (%d). Errno: %d\n", sig, siginfo->si_errno);
    execute = 0;
    return;
}

/* EOF */
