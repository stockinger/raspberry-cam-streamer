/*
 * @filename:    kill_proc.c
 * @author:      Stefan Stockinger
 * @date:        2017-01-13
 * @description: this file contains a function which kills a process by its name
*/
#define _POSIX_C_SOURCE 200112L

#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "kill_proc.h"

/**
 * @brief  returns the pid of a given process by its name.
 * @param const char* name - name of the process which should be found
 * @return  pid of the process
 *          1, if process was not found,
 *         -1, if  an reading error occured
 */
int proc_find(const char* name)
{
    DIR* dir;
    struct dirent* ent;
    char buf[512];

    long  pid;
    char pname[100] = {0,};
    char state;
    FILE *fp=NULL;

    if (!(dir = opendir("/proc"))) {
        perror("can't open /proc");
        return -1;
    }

    while((ent = readdir(dir)) != NULL) {
        long lpid = atol(ent->d_name);
        if(lpid < 0)
            continue;
        snprintf(buf, sizeof(buf), "/proc/%ld/stat", lpid);
        fp = fopen(buf, "r");

        if (fp) {
            if ( (fscanf(fp, "%ld (%[^)]) %c", &pid, pname, &state)) != 3 ) {
                printf("fscanf failed \n");
                fclose(fp);
                closedir(dir);
                return -1;
            }
            if (!strcmp(pname, name)) {
                fclose(fp);
                closedir(dir);
                return pid;
            }
            fclose(fp);
        }
    }
    closedir(dir);
    printf("no process of %s found \n", name);
    return 1;
}
