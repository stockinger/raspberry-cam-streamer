/**!
 * @file     clientControlNode.c
 * @version  V0.1
 * @date     Jan 13
 * @author   Stefan Stockinger
 *
 * @mainpage	Client clientControlNode.c
 */

#define _POSIX_C_SOURCE 200112L
/********************************************* LIBS*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
#include <pthread.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <sys/fcntl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/time.h>
/********************************************* LIBS OWN*/
#include "messageTypes.h"
/********************************************* DEFINES*/
#define MAX_POLLING_REQ 200
#define TRANSMISSION_BUFFER 255
#define IP_ADDRESS "127.0.0.1"
#define NTW_PORT 7777
/********************************************* STATIC*/
static volatile sig_atomic_t execute = 1;

static void generateCAM_DISP_CONFIG_MSQ(char *txBuffer);
static void generateCAM_CTRL_MSG(char *txBuffer);
static void generateINVALID_MSQ(char *txBuffer);
static _Bool receiveResponse(int sfd, char *rxBuffer);
/********************************************* FUNTION PROTOTYP*/
static void freeSystemRes(int sfd);
static void exitHandler(int sig, siginfo_t *siginfo, void *ignore);
/********************************************* MAIN*/
int main(int argc, char	*argv []) {
    int opt = 1;
    int cfd, iRet;

    char txBuffer[TRANSMISSION_BUFFER] = {0};
    char rxBuffer[TRANSMISSION_BUFFER] = {0};

    struct sockaddr_in saiClient;
    struct sigaction act;

    act.sa_flags = SA_SIGINFO;
    act.sa_sigaction = exitHandler;
    sigaction(SIGUSR1, &act, 0);
    sigaction(SIGINT, &act, 0);

    int random = 0;

    srand((unsigned int)time(NULL));

    //IPv4 Internet protocols
    cfd = socket(AF_INET, SOCK_STREAM, 6);
    if(cfd == -1) {
        freeSystemRes(cfd);
        (void)printf("Socket creation failed: %s\n", strerror(errno));
        (void)printf("Client has shut down!\n");
        return 1;
    }

    // Allow socket descriptor to be reusable
    iRet = setsockopt(cfd, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt));

    if(iRet == -1) {
        freeSystemRes(cfd);
        (void)printf("Socket creation failed: %s\n", strerror(errno));
        (void)printf("Client has shut down!\n");
        return 1;
    }

    (void)printf("Socket created!\n");

    //sin_family is always set to AF_INET.
    //This is required; in Linux 2.2
    saiClient.sin_family = AF_INET;
    saiClient.sin_addr.s_addr = inet_addr(IP_ADDRESS);
    saiClient.sin_port = htons(NTW_PORT);

    //Connect to camera server - In case the server is unreachable
    //the application is terminated
    iRet = connect(cfd, (struct sockaddr *)&saiClient , sizeof(saiClient));
    if (iRet == -1) {
        close(cfd);
        (void)printf("Connection CAM-Server failed: %s\n", strerror(errno));
        (void)printf("Client has shut down!\n");
        return 1;
    }

    (void)printf("Connection to Control-Node (Client) established\n");

    while(true) {
        memset(txBuffer, 0x0, sizeof(txBuffer));

        if(random == 0) {
            generateCAM_DISP_CONFIG_MSQ(txBuffer);
        } else if(random == 1) {
            generateCAM_CTRL_MSG(txBuffer);
        } else {
            generateINVALID_MSQ(txBuffer);
        }

        //Transmit message to the cam client
        iRet = send(cfd, txBuffer, sizeof(txBuffer), 0);

        if (iRet < 0) {
            (void)printf("Client: Transmission failed: %s\n",strerror(errno));
            (void)printf("Connection to client has been closed! - %d\n", cfd);
            break;
        } else {
            (void)printf("Client: MSQ was transmitted successfully\n");
        }

        memset(rxBuffer, 0x0, sizeof(rxBuffer));

        //Waiting for client response
        receiveResponse(cfd, rxBuffer);

        random++;
        sleep(2);
    }


    freeSystemRes(cfd);
    (void)printf("Client has shut down!\n");
    return 1;
}


static _Bool receiveResponse(int sfd, char *rxBuffer) {
    ssize_t iRet;

    if(rxBuffer == NULL) {
        return false;
    }

    iRet = recv(sfd, rxBuffer, sizeof(rxBuffer), 0);
    if(iRet == -1) {
        (void)printf("Message could not be received %s\n", strerror(errno));
        return false;
    } else {
        int resp;
        msg_hdr_t msgHeader;

        memcpy(&msgHeader, rxBuffer, sizeof(msg_hdr_t));

        resp = msgHeader.resp;
        (void)printf("CAM-Client Response: %d\n", resp);
        return true;
    }
}


static void generateCAM_DISP_CONFIG_MSQ(char *txBuffer) {
    msg_hdr_t msgHeader;
    cam_config_t camCfg;

    if(txBuffer == NULL) {
        return;
    }

    msgHeader.mtype = CAM_DISP_CONFIG;
    msgHeader.resp = REQ;

    camCfg.cam_cfg.cam_id = 0;
    camCfg.cam_cfg.fps = 15;
    camCfg.cam_cfg.res_x = 320;
    camCfg.cam_cfg.res_y = 240;

    memcpy(txBuffer, &msgHeader, sizeof(msg_hdr_t));
    memcpy(txBuffer + sizeof(msg_hdr_t), &camCfg, sizeof(cam_config_t));
    return;
}


static void generateCAM_CTRL_MSG(char *txBuffer) {
    msg_hdr_t msgHeader;
    cam_control_t camCtrl;

    if(txBuffer == NULL) {
        return;
    }

    msgHeader.mtype = CAM_CTRL;
    msgHeader.resp = REQ;

    camCtrl.cam_id = 0;
    camCtrl.action = START;

    memcpy(txBuffer, &msgHeader, sizeof(msg_hdr_t));
    memcpy(txBuffer + sizeof(msg_hdr_t), &camCtrl, sizeof(cam_control_t));
    return;
}


static void generateINVALID_MSQ(char *txBuffer) {
    msg_hdr_t msgHeader;
    cam_control_t camCtrl;

    if(txBuffer == NULL) {
        return;
    }

    msgHeader.mtype = AUTH_SET_PASSWORD;
    msgHeader.resp = SUCCESS;

    camCtrl.cam_id = -1;
    camCtrl.action = SLEEP;

    memcpy(txBuffer, &msgHeader, sizeof(msg_hdr_t));
    memcpy(txBuffer + sizeof(msg_hdr_t), &camCtrl, sizeof(cam_control_t));

    return;
}

/**
 * @brief Function to free the taken system resources
 * @param char *ipAdsress - used to free the allocated memory
 * @param int sfd - server file descriptor
 *
 * @return void - none
 */
void freeSystemRes(int sfd) {
    close(sfd);
    return;
}

/**
 * @brief Exit handler for the Signal ^C within the terminal.
 * @param int sig - Signal type which was received
 * @param siginfo_t *siginfo contains additional information
 * about the signal - properties
 * @param void *ignore not utilized
 *
 * @return void - none
 */
void exitHandler(int sig, siginfo_t *siginfo, void *ignore) {
    (void)printf("Received Signal: (%d). Errno: %d\n", sig, siginfo->si_errno);
    execute = 0;
    exit(0);
}

/* EOF */
