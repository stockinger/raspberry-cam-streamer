//
// Created by Stefan Stockinger on 20.01.17.
//

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "CommandLine.h"

/**
 * @brief UNITTEST_CommandLine
 *          Tests generateConsoleComand function of CommandLine.c
 */
int main(int argc, char* argv[]) {
    char message[200];

    // check for simple conversion of values
    memset(message, 0, 200);
    cam_config_t testConfig;
    testConfig.cam_cfg.cam_id = 1;
    testConfig.cam_cfg.fps = 25;
    testConfig.cam_cfg.res_x = 200;
    testConfig.cam_cfg.res_y = 100;
    char testmessage[200]= "raspivid -o - -t 0 -hf -w 200 -h 100 -fps 25 -b 1000000|cvlc -vvv stream:///dev/stdin --sout '#standard{access=http,mux=ts,dst=:8160}' :demux=h264";
    generateConsoleCommand(message, sizeof(message), testConfig);
    assert(strncmp(message, testmessage, strlen(message)-1)==0);
    printf("test successful: generation of string from correct values!\n");


    // test for insertion of default values from too low displaysize
    memset(message, 0, 200);
    testConfig.cam_cfg.cam_id = 1;
    testConfig.cam_cfg.fps = 10;
    testConfig.cam_cfg.res_x = 0;
    testConfig.cam_cfg.res_y = 0;
    char lowmessage[200]= "raspivid -o - -t 0 -hf -w 200 -h 200 -fps 10 -b 1000000|cvlc -vvv stream:///dev/stdin --sout '#standard{access=http,mux=ts,dst=:8160}' :demux=h264";
    generateConsoleCommand(message, sizeof(message), testConfig);
    assert(strncmp(message, lowmessage, strlen(message)-1)==0);
	printf("test successful: generation of string from too low values!\n");

    // test for insertion of default values from too high displaysize
    memset(message, 0, 200);
    testConfig.cam_cfg.cam_id = 1;
    testConfig.cam_cfg.fps = 10;
    testConfig.cam_cfg.res_x = 3840;
    testConfig.cam_cfg.res_y = 2160;
    char highmessage[200]= "raspivid -o - -t 0 -hf -w 200 -h 200 -fps 10 -b 1000000|cvlc -vvv stream:///dev/stdin --sout '#standard{access=http,mux=ts,dst=:8160}' :demux=h264";
    generateConsoleCommand(message, sizeof(message), testConfig);
    assert(strncmp(message, highmessage, strlen(message)-1)==0);
	printf("test successful: generation of string from too high values!\n");

    // test for no value at all
    memset(message, 0, 200);
    memset(&testConfig, 0, sizeof(testConfig));
    char voidmessage[200]= "raspivid -o - -t 0 -hf -w 200 -h 200 -fps 15 -b 1000000|cvlc -vvv stream:///dev/stdin --sout '#standard{access=http,mux=ts,dst=:8160}' :demux=h264";
    generateConsoleCommand(message, sizeof(message), testConfig);
    assert(strncmp(message, voidmessage, strlen(message)-1)==0);
    printf("test successful: generation of string from no values!\n");

    return 0;

}
