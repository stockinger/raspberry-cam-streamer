/*
 * @filename:    UNITTEST_kill_proc.c
 * @author:      Stefan Stockinger
 * @date:        2017-01-13
 * @description: this file contains a function which kills a process by its name
*/

#define _POSIX_C_SOURCE 200112L

#include <signal.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include "kill_proc.h"


int main(int argc, char* argv[]) {
    pid_t id = 0;
    
assert(proc_find("raspivid") == 1);

    pid_t procid = fork();
    if(procid == 0)
    {
        if(execl("/opt/vc/bin/raspivid","/opt/vc/bin/raspivid","-pf","baseline","-o","file",(char*) NULL) < 0) {
            printf("unable to open raspivid\n");
        } else {
            printf("raspivid started\n");
        }
    }
    else if(procid >= 0)
    {
        assert((id = proc_find("raspivid")) > 0);
	kill(id, SIGKILL);
        printf("UNITTTEST of kill_proc.c was successful\n");
    }
    return 0;
}

